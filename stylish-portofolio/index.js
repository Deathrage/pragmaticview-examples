const express = require('express');
const path = require('path');
const pragmaticView = require('pragmatic-view-express').default;

const app = express();

pragmaticView(app, {
	templateDir: path.resolve(process.cwd(), './views'),
	templateExtension: '.jsx',
	defaultLayout: './layout.jsx'
});

const router = express.Router();

router.get('/', (req, res) => {
	return void res.render('homepage');
});

app.use(express.static('fe'));
app.use(router);
app.listen(2550, () => void console.log('Listening at port: 2550'));
