# Stylish Portfolio

Very simple express app recreating Bootstrap's theme [Stylish Portfolio](https://startbootstrap.com/themes/stylish-portfolio/).

Doesn't include any advanced PragmaticView concepts.

Created using:
- [Express.JS](https://www.npmjs.com/package/express), node.js web framework 
- [PragmaticView Express](https://www.npmjs.com/package/pragmatic-view-express), plugin for Express.JS
- [PragmaticView](https://www.npmjs.com/package/pragmatic-view)