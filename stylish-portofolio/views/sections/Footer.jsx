const { View } = require("pragmatic-view");
const SocialIco = require("../components/SocialIcon");

module.exports = () => (
  <footer className="footer text-center">
    <div className="container">
      <ul className="list-inline mb-5">
        <li className="list-inline-item">
          <SocialIco ico="facebook" href="#" />
        </li>
        <li className="list-inline-item">
          <SocialIco ico="twitter" href="#" />
        </li>
        <li className="list-inline-item">
          <SocialIco ico="github" href="#" />
        </li>
      </ul>
      <p className="text-muted small mb-0">Copyright © Your Website 2019</p>
    </div>
  </footer>
);
