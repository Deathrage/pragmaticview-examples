const { View } = require("pragmatic-view");
const ServiceIcon = require("../components/ServiceIcon");

module.exports = ({ key = "" } = {}) => (
  <section
    className="content-section bg-primary text-white text-center"
    id={key}
  >
    <div className="container">
      <div className="content-section-heading">
        <h3 className="text-secondary mb-0">Services</h3>
        <h2 className="mb-5">What We Offer</h2>
      </div>
      <div className="row">
        <ServiceIcon
          ico="screen-smartphone"
          title="Responsive"
          subTitle="Looks great on any screen size!"
        />
        <ServiceIcon
          ico="pencil"
          title="Redesigned"
          subTitle="Freshly redesigned for Bootstrap 4."
        />
        <ServiceIcon
          ico="like"
          title="Favorited"
          subTitle={
            <View.Fragment>
              Millions of users
              <i className="fas fa-heart" />
              Start Bootstrap!
            </View.Fragment>
          }
        />
        <ServiceIcon
          ico="mustache"
          title="Question"
          subTitle="I mustache you a question..."
        />
      </div>
    </div>
  </section>
);
