const { View } = require("pragmatic-view");
const Link = require("../components/Link");

module.exports = () => (
  <section className="content-section bg-primary text-white">
    <div className="container text-center">
      <h2 className="mb-4">The buttons below are impossible to resist...</h2>
      <Link href="#" color="light" className="mr-4" large>
        Click Me!
      </Link>
      <Link href="#" color="dark" large>
        Look at Me!
      </Link>
    </div>
  </section>
);
