const { View } = require("pragmatic-view");
const Link = require("../components/Link");

module.exports = () => (
  <section className="callout">
    <div className="container text-center">
      <h2 className="mx-auto mb-5">
        Welcome to
        <em>your</em>
        next website!
      </h2>
      <Link
        href="https://startbootstrap.com/template-overviews/stylish-portfolio/"
        color="primary"
        large
      >
        Download Now!
      </Link>
    </div>
  </section>
);
