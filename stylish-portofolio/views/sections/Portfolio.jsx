const { View } = require("pragmatic-view");
const PortfolioItem = require("../components/PortfolioItem");

module.exports = ({ key = "" } = {}) => (
  <section className="content-section" id={key}>
    <div className="container">
      <div className="content-section-heading text-center">
        <h3 className="text-secondary mb-0">Portfolio</h3>
        <h2 className="mb-5">Recent Projects</h2>
      </div>
      <div className="row no-gutters">
        <PortfolioItem
          title="Stationary"
          subTitle="A yellow pencil with envelopes on a clean, blue backdrop!"
          img="img/portfolio-1.jpg"
        />
        <PortfolioItem
          title="Ice Cream"
          subTitle="A dark blue background with a colored pencil, a clip, and a tiny ice cream cone!"
          img="img/portfolio-2.jpg"
        />
        <PortfolioItem
          title="Strawberries"
          subTitle="Strawberries are such a tasty snack, especially with a little sugar on top!"
          img="img/portfolio-3.jpg"
        />
        <PortfolioItem
          title="Workspace"
          subTitle="A yellow workspace with some scissors, pencils, and other objects."
          img="img/portfolio-4.jpg"
        />
      </div>
    </div>
  </section>
);
