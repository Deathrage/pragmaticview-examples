const { View } = require("pragmatic-view");
const Link = require("../components/Link");

module.exports = () => (
  <header className="masthead d-flex">
    <div className="container text-center my-auto">
      <h1 className="mb-1">Stylish Portfolio</h1>
      <h3 className="mb-5">
        <em>A Free Bootstrap Theme by Start Bootstrap</em>
      </h3>
      <Link href="#about" color="primary" large scrollTrigger>
        Find Out More
      </Link>
    </div>
    <div className="overlay" />
  </header>
);
