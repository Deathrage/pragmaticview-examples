const { View } = require("pragmatic-view");
const Link = require("../components/Link");

module.exports = ({ key = "" } = {}) => (
  <section className="content-section bg-light" id={key}>
    <div className="container text-center">
      <div className="row">
        <div className="col-lg-10 mx-auto">
          <h2>Stylish Portfolio is the perfect theme for your next project!</h2>
          <p className="lead mb-5">
            This theme features a flexible, UX friendly sidebar menu and stock
            photos from our friends at
            <a href="https://unsplash.com/">Unsplash</a>!
          </p>
          <Link href="#services" color="dark" large scrollTrigger>
            What We Offer
          </Link>
        </div>
      </div>
    </div>
  </section>
);
