const { View } = require("pragmatic-view");

const Layout = () => (
  <html>
    <head>
      <meta charset="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
      <link
        href="/vendor/fontawesome-free/css/all.min.css"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="/vendor/simple-line-icons/css/simple-line-icons.css"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="/css/stylish-portfolio.min.css"
        rel="stylesheet"
        type="text/css"
      />

      <script src="vendor/jquery/jquery.min.js" />
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js" />
      <script src="vendor/jquery-easing/jquery.easing.min.js" />

      <title>Stylish Portfolio - Start Bootstrap Template</title>
    </head>
    <body id="page-top">
      <View.Placeholder />
      <script src="/js/stylish-portfolio.min.js" />
    </body>
  </html>
);

module.exports = Layout;
