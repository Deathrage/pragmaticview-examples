const { View } = require("pragmatic-view");
const AutoMenu = require("./components/AutoMenu");
const Home = require("./sections/Home");
const About = require("./sections/About");
const Services = require("./sections/Services");
const Portfolio = require("./sections/Portfolio");
const Callout = require("./sections/Callout");
const ImpossibleButtons = require("./sections/ImpossibleButtons");
const Contact = require("./sections/Contact");
const Footer = require("./sections/Footer");

const Homepage = () => (
  <AutoMenu>
    <Home />
    <About key="about" title="About" />
    <Services key="services" title="Services" />
    <Callout />
    <Portfolio key="portfolio" title="Portfolio" />
    <ImpossibleButtons />
    <Contact key="contact" title="Contact" />
    <Footer />
  </AutoMenu>
);

module.exports = Homepage;
