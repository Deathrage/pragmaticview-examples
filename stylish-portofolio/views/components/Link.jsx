const { View } = require("pragmatic-view");
const classNamesFunc = require("classnames");

module.exports = ({
  children,
  href = "",
  large = false,
  color = "primary",
  scrollTrigger = false,
  className = ""
}) => {
  const classNames = classNamesFunc(
    "btn",
    {
      "js-scroll-trigger": scrollTrigger,
      "btn-xl": large
    },
    `btn-${color}`,
    className
  );
  return (
    <a className={classNames} href={href}>
      {children}
    </a>
  );
};
