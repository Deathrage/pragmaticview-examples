const { View } = require("pragmatic-view");

module.exports = ({ title = "", subTitle = "", img = "" } = {}) => (
  <div className="col-lg-6">
    <a className="portfolio-item" href="#">
      <span className="caption">
        <span className="caption-content">
          <h2>{title}</h2>
          <p className="mb-0">{subTitle}</p>
        </span>
      </span>
      {img && <img className="img-fluid" src={img} alt={title} />}
    </a>
  </div>
);
