const { View } = require("pragmatic-view");

module.exports = ({ ico = "", href = "" }) => (
  <a className="social-link rounded-circle text-white mr-3" href={href}>
    <i className={`icon-social-${ico}`} />
  </a>
);
