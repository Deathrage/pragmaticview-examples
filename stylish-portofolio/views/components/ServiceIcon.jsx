const { View } = require("pragmatic-view");

module.exports = ({ title = "", subTitle = "", ico = "" }) => (
  <div className="col-lg-3 col-md-6 mb-5 mb-lg-0">
    <span className="service-icon rounded-circle mx-auto mb-3">
      <i className={`icon-${ico}`} />
    </span>
    <h4>
      <strong>{title}</strong>
    </h4>
    <p className="text-faded mb-0">{subTitle}</p>
  </div>
);
