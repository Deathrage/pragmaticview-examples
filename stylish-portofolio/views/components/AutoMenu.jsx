const { View } = require("pragmatic-view");
const {
  predicate: { isElement },
  elements: { extractProps }
} = require("pragmatic-view").Helpers;

class AutoMenu extends View.Component {
  onInit() {
    // Filter out elements and extract their props
    this.menuItems = this.props.children
      .filter(isElement)
      .map(child => {
        const { key, title } = extractProps(child);
        return {
          key,
          title
        };
      })
      .filter(item => item.key && item.title);
  }

  render() {
    return (
      <View.Fragment>
        <a className="menu-toggle rounded" href="#">
          <i className="fas fa-bars" />
        </a>
        <nav id="sidebar-wrapper">
          <ul className="sidebar-nav">
            <li className="sidebar-brand">
              <a className="js-scroll-trigger" href="#page-top">
                Start Bootstrap
              </a>
            </li>
            <li className="sidebar-nav-item">
              <a className="js-scroll-trigger" href="#page-top">
                Home
              </a>
            </li>
            {this.menuItems.map(({ key, title }) => (
              <li className="sidebar-nav-item">
                <a className="js-scroll-trigger" href={`#${key}`}>
                  {title}
                </a>
              </li>
            ))}
          </ul>
        </nav>
        {this.props.children}
        <a
          className="scroll-to-top rounded js-scroll-trigger"
          href="#page-top"
          style="display: inline;"
        >
          <i className="fas fa-angle-up" />
        </a>
      </View.Fragment>
    );
  }
}

module.exports = AutoMenu;
